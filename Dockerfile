FROM maven:3-openjdk-11-slim
ENV DB_URL_VAR = ""
ENV DB_USERNAME_VAR = ""
ENV DB_PASSWORD_VAR = ""
COPY src /src
COPY pom.xml /
RUN mvn -f pom.xml clean package
RUN mv /target/*.jar app.jar
ENTRYPOINT ["java","-jar","/app.jar", "-DB_URL=${DB_URL_VAR}", "DB_USERNAME=${DB_USERNAME_VAR}", "DB_PASSWORD=${DB_PASSWORD_VAR}"]
