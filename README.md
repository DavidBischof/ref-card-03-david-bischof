# README - Refcard03-Pipelines
Dieses README enthält alle Informationen und Anweisungen, die erforderlich sind, um das Projekt mit der Pipeline vollständig zu verstehen und die gewünschte Funktionalität zu erreichen. Bitte lesen Sie dieses README sorgfältig durch, um sicherzustellen, dass Sie alle erforderlichen Schritte befolgen.

# Pipeline 

In diesem Projekt wird eine Pipeline verwendet, um eine Verbindung zwischen dem Repository (GitLab) und dem ECR aufzubauen. Die Pipeline ermöglicht es, dass das gesamte Projekt automatisch in eine Container-Image-Build-Pipeline integriert wird und anschließend mithilfe von ECS bereitgestellt werden kann.

# Automatisierung 

## Hier ist eine kurze Erläuterung des Prozesses:

1. Commit und Push: Entwicklerinnen und Entwickler commiten ihre Code-Änderungen in das GitLab-Repository und pushen sie in den entsprechenden Branch.

2. CI/CD-Pipeline-Auslösung: Der Push in das Repository löst die CI/CD-Pipeline aus. Die Pipeline besteht aus mehreren Schritten, um den Code zu überprüfen, Tests auszuführen und das Container-Image zu erstellen.

3. Container-Build: In der Pipeline wird das Repository geklont und der Code innerhalb eines Container-Images gebaut.

4. Container-Image-Push: Das erstellte Container-Image wird in den Elastic Container Registry  hochgeladen.

5. ECS-Bereitstellung: Der aktualisierte Code wird automatisch in eine ECS-Clusterumgebung bereitgestellt.

Durch die Verwendung der Pipeline wird der Prozess der Codebereitstellung automatisiert, was eine schnellere Entwicklung, Testung und Bereitstellung ermöglicht. Entwicklerinnen und Entwickler können sich auf die Entwicklung konzentrieren, während die Pipeline den Build-, Test- und Bereitstellungsprozess übernimmt.

# CI/CD-Variablen (GitLab)
Für die Konfiguration der CI/ CD in GitLab sind bestimmte Umgebungsvariablen erforderlich. Bitte stellen Sie sicher, dass die folgenden Variablen in Ihrem GitLab-Projekt definiert sind:

CI_AWS_ECR_REGISTRY: Für die Verbindung mit dem ECR nötig. <br>
CI_AWS_ECR_REPOSITORY_NAME: Der Name des obrigen ECR. <br>
CI_AWS_ECS_CLUSTER_NAME: Name des Cluster <br>
CI_AWS_ECS_SERVICE_NAME: Name des Service <br>
AWS_ACCESS_KEY_ID: Dies ändert sich bei jedem Neustart und ist die AWS ID. <br>
AWS_DEFAULT_REGION: Wird warscheindlich nicht verändert und ist die Region von AWS nötig. <br>
AWS_SECRET_ACCESS_KEY: Wir ebenfalls bei neustart verändert und ist für den Zugriff nötig. <br>
AWS_SESSION_TOKEN: Wir bei Neustart verändert und ist für die jetzige Sitzung verwendet. <br>




# Entwicklungsumgebung
Um an diesem Projekt mitzuentwickeln, müssen Sie die folgenden Schritte ausführen, um Ihre Entwicklungsumgebung einzurichten:

Clone des Repositories: 
1. Führen Sie den Befehl git clone <https://gitlab.com/DavidBischof/ref-card-03-david-bischof> aus, um eine lokale Kopie des Projekts zu erhalten.
2. Installieren Sie die erforderlichen Abhängigkeiten
3. Konfiguration der Umgebungsvariablen: Erstellen Sie eine Datei namens .env im Hauptverzeichnis des Projekts und definieren Sie die erforderlichen Umgebungsvariablen gemäß den zuvor genannten Anforderungen.
4. Führen Sie das Projekt aus
Ihrem lokalen Entwicklungsrechner.

## Beitrag zur Entwicklung
Wenn Sie zu diesem Projekt beitragen möchten, folgen Sie bitte den nachstehenden Schritten:

1. Erstellen Sie einen Fork des Repositorys.
2. Erstellen Sie einen neuen Branch: git checkout -b feature/your-feature-name.
3. Führen Sie Ihre Änderungen durch und commiten Sie diese: git commit -m "Beschreibung der Änderungen".
4. Pushen Sie Ihre Änderungen in Ihren Fork: git push origin feature/your-feature-name.
5. Erstellen Sie einen Pull Request in das Hauptrepository.
6. Bitte stellen Sie sicher, dass Ihre Änderungen gut getestet sind und den Qualitätsstandards des Projekts entsprechen.
# Beiträge

Ein Danke für die folgenden personen, welche bei diesem Projekt mitgeholfen haben:

* [@DavidBischof](https://gitlab.com/DavidBischof) 📖
* [@doxic](https://gitlab.com/doxic) 📖
* [@bbwrl ](https://gitlab.com/bbwrl) 📖

# Kontakt

 <david.bischof@lernende.bbw.ch> or <bischof.david.db@gmail.com>.

